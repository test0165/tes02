import flask
from flask import render_template
import pickle
import sklearn
from sklearn.linear_model import LinearRegression

app = flask.Flask(__name__, template_folder = 'templates')

@app.route('/', methods = ['POST', 'GET'])

@app.route('/index', methods = ['POST', 'GET'])
def main():
  if flask.request.method == 'GET':
    return render_template('main.html')
  if flask.request.method == 'POST':
    model_load = pickle.load(open('lr_model.pki', 'rb'))
    exp = float(flask.request.form['experience'])
    y_pred = model_load.predict([[exp]])
    return render_template('main.html', result = y_pred[0][0])

if __name__ == '__main__':
  app.run()
